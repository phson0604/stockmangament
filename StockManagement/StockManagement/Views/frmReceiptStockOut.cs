﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using StockManagement.Model;
using RestSharp;
using Newtonsoft.Json;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraReports.UI;
using DevExpress.XtraEditors.Repository;

namespace StockManagement
{
    public partial class frmReceiptStockOut : DevExpress.XtraEditors.XtraForm
    {
        POItems Items = new POItems();
        List<StockOut> st = new List<StockOut>();
        string[] gender = { "Hà Nội", "Hải Phòng" };
        string[] pos = { "302 Cầu Giấy", "330 Phủ Thượng Đoạn,Đông Hải 1,Hải An" };
        string[] unit = { "Cái", "Hộp", "Chiếc", "Kg" };
        List<POItem> qt = new List<POItem>();
        public frmReceiptStockOut()
        {
            InitializeComponent();
        }

        private void frmReceiptStockOut_Load(object sender, EventArgs e)
        {
            
           
            string content = RestSharpC.execCommand("POs/getAll", Method.GET);
            JsonHeadStockOut result = JsonConvert.DeserializeObject<JsonHeadStockOut>(content);
            //grStockIn.DataSource = result.Data;
            cbPo.DataSource = result.Data.Select(s => s.Id).ToList();
            foreach (string genders in gender)
            {
                //cmbStore.Items.Add(genders);
                cbStore.Items.Add(genders);
            }
            foreach (string poss in pos)
            {

                cbPosition.Items.Add(poss);
            }
            foreach (string units in unit)
            {

                cbUnit.Items.Add(units);
            }
        }

        private void btnSearchPO_Click(object sender, EventArgs e)
        {
            int id = int.Parse(cbPo.Text);
            var content = RestSharpC.execCommand5("POItems/getPo/{poID}", Method.GET, id);
            JsonHeadPOItem result = JsonConvert.DeserializeObject<JsonHeadPOItem>(content);
            //List<QuoationItem> lst = new List<QuoationItem>();
            //lst.Add(result.Data);
            Items = result.Data;
            gridControl1.DataSource = Items;
            
        }

        private void btnDeleteItemStockOut_Click(object sender, EventArgs e)
        {
            gridView1.DeleteRow(gridView1.FocusedRowHandle);
        }

        private void btnAddStockOut_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                POItem r = gridView1.GetRow(i) as POItem;

                var Cus = new StockOut
                {
                    PoID = r.PoID,
                    PartNumber = r.PartNumber,
                    Quantity = r.Quantity,
                    Unit=r.Unit,
                    Date = dtpDate.Value,
                    Note = txtNote.Text,
                    ReceiptNo = txtReceiptNo.Text,
                    Evidence = txtEvidence.Text,
                    ActualNumber = r.ActualNumber,
                    Store = cbStore.Text,
                    Position=cbPosition.Text
                };
                st.Add(Cus);
            }

            RestSharpC.execCommand6("StockOuts", Method.POST, st.ToArray());
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = sender as GridView;
            if (view.FocusedColumn.FieldName == "PartNumber")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
            if (view.FocusedColumn.FieldName == "Id")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
            if (view.FocusedColumn.FieldName == "PoID")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
            if (view.FocusedColumn.FieldName == "Evidence")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
            if (view.FocusedColumn.FieldName == "UnitPrice")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
            if (view.FocusedColumn.FieldName == "ReceiptNo")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
            if (view.FocusedColumn.FieldName == "PartName")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
            if (view.FocusedColumn.FieldName == "Currency")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
        }

        private void btnScanBarcode_Click(object sender, EventArgs e)
        {
            frmScanBarcode scb = new frmScanBarcode();
            scb.ShowDialog();
        }

        private void btnPrintReport_Click(object sender, EventArgs e)
        {
            int id = int.Parse(cbPo.Text);
            var content1 = RestSharpC.execCommand1("POs/{id}", Method.GET, id);
            JsonPO result1 = JsonConvert.DeserializeObject<JsonPO>(content1);
            lbCustomer.Text = result1.Data.Customers.NAME.ToString();
            //var content = RestSharpC.execCommand5("POItems/getPo/{poID}", Method.GET, id);
            //JsonHeadPOItem result = JsonConvert.DeserializeObject<JsonHeadPOItem>(content);
            ////List<QuoationItem> lst = new List<QuoationItem>();
            ////lst.Add(result.Data);
            //Items = result.Data;
            //gridControl1.DataSource = Items;
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                POItem r = gridView1.GetRow(i) as POItem;

                var Cus = new POItem
                {
                    PoID = r.PoID,
                    PartNumber = r.PartNumber,
                    Quantity = r.Quantity,
                    Unit = r.Unit,
                    PartName=r.PartName,
                    //Date = dtpDate.Value,
                    //Note = txtNote.Text,
                    //ReceiptNo = txtReceiptNo.Text,
                    //Evidence = txtEvidence.Text,
                    UnitPrice = r.UnitPrice,
                    ActualNumber = r.ActualNumber,
                    Store = cbStore.Text,
                    Position = cbPosition.Text
                };
                qt.Add(Cus);
            }
            rpStockOut rp = new rpStockOut(qt,lbCustomer.Text,txtNameStockOut.Text,txtReceiptNo.Text,txtNoStockIn.Text);
            rp.ExportToPdf("ab.pdf");
            ReportPrintTool tool = new ReportPrintTool(rp);
            tool.ShowPreviewDialog();
        }

        private void cbStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbStore.SelectedItem.ToString() == "Hà Nội")
            {
                cbPosition.SelectedItem = "302 Cầu Giấy";
            }
            else
            {
                cbPosition.SelectedItem = "330 Phủ Thượng Đoạn,Đông Hải 1,Hải An";
            }
        }
    }
}