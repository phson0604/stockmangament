﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RestSharp;
using Newtonsoft.Json;
using DevExpress.XtraGrid.Views.Grid;
using StockManagement.Model;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace StockManagement
{
    public partial class frmReceiptStockIns : DevExpress.XtraEditors.XtraForm
    {
        //private String URL = "http://localhost:8000/api/v1/";
        public static string SetValueForText1 = "";
        public static string SetValueForText2 = "";
        QuoationItems Items = new QuoationItems();     
        List<StockIn> st = new List<StockIn>();
        List<QuoationItem> qt = new List<QuoationItem>();
        string[] gender = { "Hà Nội", "Hải Phòng" };
        string[] pos = { "302 Cầu Giấy", "330 Phủ Thượng Đoạn,Đông Hải 1,Hải An" };
        string[] unit = {"Cái" ,"Hộp","Chiếc","Kg"};
        public frmReceiptStockIns()
        {
            InitializeComponent();
        }
        
        private void frmReceiptStockIns_Load(object sender, EventArgs e)
        {
            string content = RestSharpC.execCommand("Quotations/getAll", Method.GET);
            JsonHeadQuoation result = JsonConvert.DeserializeObject<JsonHeadQuoation>(content);
            //grStockIn.DataSource = result.Data;
            comboBox1.DataSource = result.Data.Select(s=>s.Id).ToList();
            foreach(string genders in gender)
            {
                //cmbStore.Items.Add(genders);
                cbStore.Items.Add(genders);
            }
            foreach (string poss in pos)
            {

                cbPostion.Items.Add(poss);
            }
            foreach (string units in unit)
            {

                cmbUnit.Items.Add(units);
            }
        }

        private void btnQuation_Click(object sender, EventArgs e)
        {
            try
            {
                int id = int.Parse(comboBox1.Text);
                var content = RestSharpC.execCommand2("QuotationItems/getQo/{quotationID}", Method.GET, id);
                JsonHeadQuoationItem result = JsonConvert.DeserializeObject<JsonHeadQuoationItem>(content);
                //List<QuoationItem> lst = new List<QuoationItem>();
                //lst.Add(result.Data);
                Items = result.Data;
                gridControl1.DataSource = Items;
            }
            catch (Exception error)
            {
                MessageBox.Show("Co loi roi" + error);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                QuoationItem r = gridView1.GetRow(i) as QuoationItem;
                var Cus = new StockIn
                {
                    QuotationID = r.QuotationID,
                    PartNumber = r.PartNumber,
                    Quantity = r.Quantity,
                    Date = dtpDate.Value,
                    Note = txtNote.Text,
                    ReceiptNo = txtReceiptNo.Text,
                    Unit = r.Unit,
                    ActualNumber= r.ActualNumber,
                    Store=cbStore.Text,
                    Position=cbPostion.Text
                };
                st.Add(Cus);
            }
           RestSharpC.execCommand4("StockIns", Method.POST, st.ToArray());
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            gridView1.DeleteRow(gridView1.FocusedRowHandle);
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = sender as GridView;
            if (view.FocusedColumn.FieldName == "PartNumber")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
            if (view.FocusedColumn.FieldName == "Id")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
            if (view.FocusedColumn.FieldName == "QuotationID")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
            if (view.FocusedColumn.FieldName == "PartName")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
            if (view.FocusedColumn.FieldName == "UnitPrice")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
            if (view.FocusedColumn.FieldName == "Currency")//&& !USCanada(view, view.FocusedRowHandle))
                e.Cancel = true;
        }

        private void btnPrintBarcode_Click(object sender, EventArgs e)
        {
            QuoationItem r = gridView1.GetRow(1) as QuoationItem;
            frmStockIn h = new frmStockIn();            
            var Cus = new StockIn
            {
                QuotationID = r.QuotationID,
                PartNumber = r.PartNumber,
                Quantity = r.Quantity,
                Date = dtpDate.Value
            };
            frmBarcode b1 = new frmBarcode();
            SetValueForText1 = comboBox1.Text;
            SetValueForText2 = Cus.Date.ToString("yyyyMMdd");          
            b1.Show();           
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            try
            {
                int id = int.Parse(comboBox1.Text);
                var content1 = RestSharpC.execCommand1("Quotations/{id}", Method.GET, id);
                JsonQuoation result1 = JsonConvert.DeserializeObject<JsonQuoation>(content1);
                txtSupplier.Text = result1.Data.Suppliers.NAME.ToString();
                //var content = RestSharpC.execCommand2("QuotationItems/getQo/{quotationID}", Method.GET, id);
                //JsonHeadQuoationItem result = JsonConvert.DeserializeObject<JsonHeadQuoationItem>(content);
                //Items = result.Data;
                //gridControl1.DataSource = Items;
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    QuoationItem r = gridView1.GetRow(i) as QuoationItem;
                    
                    var Cus = new QuoationItem
                    {
                        QuotationID = r.QuotationID,
                        PartNumber = r.PartNumber,
                        PartName = r.PartName,
                        ActualNumber = r.ActualNumber,
                        Quantity=r.Quantity,
                        Currency=r.Currency,
                        UnitPrice=r.UnitPrice,
                        Unit=r.Unit,
                        Store = cbStore.Text,
                        Position = cbPostion.Text


                    };
                    qt.Add(Cus);
                    
                }
                
                rpStockIn rp = new rpStockIn(qt, txtSupplier.Text,txtReceiptNo.Text,txtNameStockIn.Text,txtNoStockOut.Text);
                ReportPrintTool tool = new ReportPrintTool(rp);
                tool.ShowPreviewDialog();
            }
            catch (Exception error)
            {
                MessageBox.Show("Co loi roi" + error);
            }
        }

        private void cbStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbStore.SelectedItem.ToString() == "Hà Nội")
            {
                cbPostion.SelectedItem = "302 Cầu Giấy";
            }
            else
            {
                cbPostion.SelectedItem = "330 Phủ Thượng Đoạn,Đông Hải 1,Hải An";
            }     
                
            
        }
    }
}