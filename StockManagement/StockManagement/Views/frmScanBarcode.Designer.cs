﻿namespace StockManagement
{
    partial class frmScanBarcode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmScanBarcode));
            this.btnScan = new DevExpress.XtraEditors.SimpleButton();
            this.txtScanBarcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnScan
            // 
            this.btnScan.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnScan.ImageOptions.SvgImage")));
            this.btnScan.Location = new System.Drawing.Point(168, 119);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(131, 44);
            this.btnScan.TabIndex = 2;
            this.btnScan.Text = "Scan";
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // txtScanBarcode
            // 
            this.txtScanBarcode.Location = new System.Drawing.Point(168, 65);
            this.txtScanBarcode.Name = "txtScanBarcode";
            this.txtScanBarcode.Size = new System.Drawing.Size(189, 27);
            this.txtScanBarcode.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "Scan Barcode";
            // 
            // frmScanBarcode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 205);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtScanBarcode);
            this.Controls.Add(this.btnScan);
            this.IconOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("frmScanBarcode.IconOptions.LargeImage")));
            this.Name = "frmScanBarcode";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quét Mã Barcode";
            this.Load += new System.EventHandler(this.frmScanBarcode_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnScan;
        private System.Windows.Forms.TextBox txtScanBarcode;
        private System.Windows.Forms.Label label1;
    }
}