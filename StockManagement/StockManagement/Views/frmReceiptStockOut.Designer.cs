﻿namespace StockManagement
{
    partial class frmReceiptStockOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReceiptStockOut));
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.pOItemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cbUnit = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colCurrency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNoStockIn = new System.Windows.Forms.TextBox();
            this.txtNameStockOut = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbPosition = new System.Windows.Forms.ComboBox();
            this.cbStore = new System.Windows.Forms.ComboBox();
            this.lbCustomer = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEvidence = new System.Windows.Forms.TextBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.txtReceiptNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPo = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnPrintReport = new DevExpress.XtraEditors.SimpleButton();
            this.btnScanBarcode = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddStockOut = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteItemStockOut = new DevExpress.XtraEditors.SimpleButton();
            this.btnSearchPO = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOItemsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbUnit)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.pOItemsBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gridControl1.Location = new System.Drawing.Point(0, 295);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.cbUnit});
            this.gridControl1.Size = new System.Drawing.Size(1067, 200);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // pOItemsBindingSource
            // 
            this.pOItemsBindingSource.DataSource = typeof(StockManagement.POItems);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colPoID,
            this.colPartNumber,
            this.colPartName,
            this.colQuantity,
            this.colUnitPrice,
            this.colActualNumber,
            this.colUnit,
            this.colCurrency});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.MinWidth = 25;
            this.colId.Name = "colId";
            this.colId.Visible = true;
            this.colId.VisibleIndex = 0;
            this.colId.Width = 94;
            // 
            // colPoID
            // 
            this.colPoID.FieldName = "PoID";
            this.colPoID.MinWidth = 25;
            this.colPoID.Name = "colPoID";
            this.colPoID.Visible = true;
            this.colPoID.VisibleIndex = 1;
            this.colPoID.Width = 94;
            // 
            // colPartNumber
            // 
            this.colPartNumber.FieldName = "PartNumber";
            this.colPartNumber.MinWidth = 25;
            this.colPartNumber.Name = "colPartNumber";
            this.colPartNumber.Visible = true;
            this.colPartNumber.VisibleIndex = 2;
            this.colPartNumber.Width = 94;
            // 
            // colPartName
            // 
            this.colPartName.FieldName = "PartName";
            this.colPartName.MinWidth = 25;
            this.colPartName.Name = "colPartName";
            this.colPartName.Visible = true;
            this.colPartName.VisibleIndex = 3;
            this.colPartName.Width = 94;
            // 
            // colQuantity
            // 
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.MinWidth = 25;
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 4;
            this.colQuantity.Width = 94;
            // 
            // colUnitPrice
            // 
            this.colUnitPrice.FieldName = "UnitPrice";
            this.colUnitPrice.MinWidth = 25;
            this.colUnitPrice.Name = "colUnitPrice";
            this.colUnitPrice.Visible = true;
            this.colUnitPrice.VisibleIndex = 5;
            this.colUnitPrice.Width = 94;
            // 
            // colActualNumber
            // 
            this.colActualNumber.FieldName = "ActualNumber";
            this.colActualNumber.MinWidth = 25;
            this.colActualNumber.Name = "colActualNumber";
            this.colActualNumber.Visible = true;
            this.colActualNumber.VisibleIndex = 6;
            this.colActualNumber.Width = 94;
            // 
            // colUnit
            // 
            this.colUnit.ColumnEdit = this.cbUnit;
            this.colUnit.FieldName = "Unit";
            this.colUnit.MinWidth = 25;
            this.colUnit.Name = "colUnit";
            this.colUnit.Visible = true;
            this.colUnit.VisibleIndex = 7;
            this.colUnit.Width = 94;
            // 
            // cbUnit
            // 
            this.cbUnit.AutoHeight = false;
            this.cbUnit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbUnit.Name = "cbUnit";
            // 
            // colCurrency
            // 
            this.colCurrency.FieldName = "Currency";
            this.colCurrency.MinWidth = 25;
            this.colCurrency.Name = "colCurrency";
            this.colCurrency.Visible = true;
            this.colCurrency.VisibleIndex = 8;
            this.colCurrency.Width = 94;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbPosition);
            this.groupBox1.Controls.Add(this.cbStore);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtEvidence);
            this.groupBox1.Controls.Add(this.txtNote);
            this.groupBox1.Controls.Add(this.dtpDate);
            this.groupBox1.Controls.Add(this.txtReceiptNo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbPo);
            this.groupBox1.Location = new System.Drawing.Point(21, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(760, 173);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thong tin";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 90);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 19);
            this.label9.TabIndex = 20;
            this.label9.Text = "No StockIn";
            // 
            // txtNoStockIn
            // 
            this.txtNoStockIn.Location = new System.Drawing.Point(106, 87);
            this.txtNoStockIn.Name = "txtNoStockIn";
            this.txtNoStockIn.Size = new System.Drawing.Size(148, 27);
            this.txtNoStockIn.TabIndex = 19;
            // 
            // txtNameStockOut
            // 
            this.txtNameStockOut.Location = new System.Drawing.Point(106, 34);
            this.txtNameStockOut.Name = "txtNameStockOut";
            this.txtNameStockOut.Size = new System.Drawing.Size(148, 27);
            this.txtNameStockOut.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 19);
            this.label6.TabIndex = 17;
            this.label6.Text = "Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(527, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 19);
            this.label8.TabIndex = 16;
            this.label8.Text = "Position";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(527, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 19);
            this.label7.TabIndex = 15;
            this.label7.Text = "Store";
            // 
            // cbPosition
            // 
            this.cbPosition.FormattingEnabled = true;
            this.cbPosition.Location = new System.Drawing.Point(599, 84);
            this.cbPosition.Name = "cbPosition";
            this.cbPosition.Size = new System.Drawing.Size(121, 27);
            this.cbPosition.TabIndex = 14;
            // 
            // cbStore
            // 
            this.cbStore.FormattingEnabled = true;
            this.cbStore.Location = new System.Drawing.Point(599, 39);
            this.cbStore.Name = "cbStore";
            this.cbStore.Size = new System.Drawing.Size(121, 27);
            this.cbStore.TabIndex = 13;
            this.cbStore.SelectedIndexChanged += new System.EventHandler(this.cbStore_SelectedIndexChanged);
            // 
            // lbCustomer
            // 
            this.lbCustomer.AutoSize = true;
            this.lbCustomer.Location = new System.Drawing.Point(14, 135);
            this.lbCustomer.Name = "lbCustomer";
            this.lbCustomer.Size = new System.Drawing.Size(51, 19);
            this.lbCustomer.TabIndex = 12;
            this.lbCustomer.Text = "label7";
            this.lbCustomer.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 19);
            this.label5.TabIndex = 9;
            this.label5.Text = "Note";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(266, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 19);
            this.label4.TabIndex = 8;
            this.label4.Text = "Evidence";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(266, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 19);
            this.label3.TabIndex = 7;
            this.label3.Text = "Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 19);
            this.label2.TabIndex = 6;
            this.label2.Text = "Receipt No";
            // 
            // txtEvidence
            // 
            this.txtEvidence.Location = new System.Drawing.Point(343, 84);
            this.txtEvidence.Name = "txtEvidence";
            this.txtEvidence.Size = new System.Drawing.Size(168, 27);
            this.txtEvidence.TabIndex = 5;
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(107, 135);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(613, 27);
            this.txtNote.TabIndex = 4;
            // 
            // dtpDate
            // 
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDate.Location = new System.Drawing.Point(343, 39);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(168, 27);
            this.dtpDate.TabIndex = 3;
            // 
            // txtReceiptNo
            // 
            this.txtReceiptNo.Location = new System.Drawing.Point(107, 87);
            this.txtReceiptNo.Name = "txtReceiptNo";
            this.txtReceiptNo.Size = new System.Drawing.Size(135, 27);
            this.txtReceiptNo.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "PO";
            // 
            // cbPo
            // 
            this.cbPo.FormattingEnabled = true;
            this.cbPo.Location = new System.Drawing.Point(107, 39);
            this.cbPo.Name = "cbPo";
            this.cbPo.Size = new System.Drawing.Size(135, 27);
            this.cbPo.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnPrintReport);
            this.groupBox2.Controls.Add(this.btnScanBarcode);
            this.groupBox2.Controls.Add(this.btnAddStockOut);
            this.groupBox2.Controls.Add(this.btnDeleteItemStockOut);
            this.groupBox2.Controls.Add(this.btnSearchPO);
            this.groupBox2.Location = new System.Drawing.Point(21, 191);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1087, 98);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chuc Nang";
            // 
            // btnPrintReport
            // 
            this.btnPrintReport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintReport.ImageOptions.Image")));
            this.btnPrintReport.Location = new System.Drawing.Point(829, 38);
            this.btnPrintReport.Name = "btnPrintReport";
            this.btnPrintReport.Size = new System.Drawing.Size(160, 42);
            this.btnPrintReport.TabIndex = 4;
            this.btnPrintReport.Text = "In Phiếu Xuất";
            this.btnPrintReport.Click += new System.EventHandler(this.btnPrintReport_Click);
            // 
            // btnScanBarcode
            // 
            this.btnScanBarcode.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnScanBarcode.ImageOptions.SvgImage")));
            this.btnScanBarcode.Location = new System.Drawing.Point(622, 38);
            this.btnScanBarcode.Name = "btnScanBarcode";
            this.btnScanBarcode.Size = new System.Drawing.Size(154, 42);
            this.btnScanBarcode.TabIndex = 3;
            this.btnScanBarcode.Text = "Scan Barcode";
            this.btnScanBarcode.Click += new System.EventHandler(this.btnScanBarcode_Click);
            // 
            // btnAddStockOut
            // 
            this.btnAddStockOut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddStockOut.ImageOptions.Image")));
            this.btnAddStockOut.Location = new System.Drawing.Point(430, 38);
            this.btnAddStockOut.Name = "btnAddStockOut";
            this.btnAddStockOut.Size = new System.Drawing.Size(128, 42);
            this.btnAddStockOut.TabIndex = 2;
            this.btnAddStockOut.Text = "Thêm Mới";
            this.btnAddStockOut.Click += new System.EventHandler(this.btnAddStockOut_Click);
            // 
            // btnDeleteItemStockOut
            // 
            this.btnDeleteItemStockOut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteItemStockOut.ImageOptions.Image")));
            this.btnDeleteItemStockOut.Location = new System.Drawing.Point(242, 38);
            this.btnDeleteItemStockOut.Name = "btnDeleteItemStockOut";
            this.btnDeleteItemStockOut.Size = new System.Drawing.Size(125, 42);
            this.btnDeleteItemStockOut.TabIndex = 1;
            this.btnDeleteItemStockOut.Text = "Xóa";
            this.btnDeleteItemStockOut.Click += new System.EventHandler(this.btnDeleteItemStockOut_Click);
            // 
            // btnSearchPO
            // 
            this.btnSearchPO.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchPO.ImageOptions.Image")));
            this.btnSearchPO.Location = new System.Drawing.Point(59, 38);
            this.btnSearchPO.Name = "btnSearchPO";
            this.btnSearchPO.Size = new System.Drawing.Size(143, 42);
            this.btnSearchPO.TabIndex = 0;
            this.btnSearchPO.Text = "Chọn";
            this.btnSearchPO.Click += new System.EventHandler(this.btnSearchPO_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.txtNameStockOut);
            this.groupBox3.Controls.Add(this.txtNoStockIn);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.lbCustomer);
            this.groupBox3.Location = new System.Drawing.Point(787, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(268, 173);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "In Phiếu Xuất";
            // 
            // frmReceiptStockOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 495);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gridControl1);
            this.IconOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("frmReceiptStockOut.IconOptions.LargeImage")));
            this.Name = "frmReceiptStockOut";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phiếu Xuất";
            this.Load += new System.EventHandler(this.frmReceiptStockOut_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOItemsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbUnit)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbPo;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.SimpleButton btnSearchPO;
        private DevExpress.XtraEditors.SimpleButton btnDeleteItemStockOut;
        private DevExpress.XtraEditors.SimpleButton btnAddStockOut;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.TextBox txtReceiptNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEvidence;
        private DevExpress.XtraEditors.SimpleButton btnScanBarcode;
        private DevExpress.XtraEditors.SimpleButton btnPrintReport;
        public System.Windows.Forms.Label lbCustomer;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbPosition;
        private System.Windows.Forms.ComboBox cbStore;
        private System.Windows.Forms.BindingSource pOItemsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colPoID;
        private DevExpress.XtraGrid.Columns.GridColumn colPartNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPartName;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colActualNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbUnit;
        private System.Windows.Forms.TextBox txtNameStockOut;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNoStockIn;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}