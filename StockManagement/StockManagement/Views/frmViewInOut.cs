﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RestSharp;
using StockManagement.Model;
using Newtonsoft.Json;

namespace StockManagement
{
    public partial class frmViewInOut : DevExpress.XtraEditors.XtraForm
    {
        private String URL = "http://localhost:8000/api/v1/";
        public frmViewInOut()
        {
            InitializeComponent();
        }

        private string execCommand(string source, Method method, string partNumber)
        {
            RestClient client = new RestClient(URL);
            RestRequest request = new RestRequest(source, method);
            request.AddUrlSegment("partNumber", partNumber);
            request.RequestFormat = DataFormat.Json;
            IRestResponse response = client.Execute(request);  // Execute the Request
            string content = response.Content;
            return content;
        }           
        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try {
                string id = txtKetQua.Text;
                var content = execCommand("StockIns/getItems/{partNumber}", Method.GET, id);
                JsonHeadStockIn result = JsonConvert.DeserializeObject<JsonHeadStockIn>(content);
                grConTrolInOut.DataSource = result.Data;
            }
            catch(Exception error)
            {
                MessageBox.Show("Co loi nhap lieu" +error);
            }
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string id = txtKetQua.Text;
                var content = execCommand("StockOuts/getItems/{partNumber}", Method.GET, id);
                JsonHeadStockOut result = JsonConvert.DeserializeObject<JsonHeadStockOut>(content);
                grConTrolInOut.DataSource = result.Data;
            }
            catch(Exception error)
            {
                MessageBox.Show("Co loi roi"+error);
            }
        }
    }
}