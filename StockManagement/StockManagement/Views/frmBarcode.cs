﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BC_gen;
using System.Net;
using System.IO;
using RestSharp;
using Newtonsoft.Json;
using StockManagement.Model;

namespace StockManagement
{
    public partial class frmBarcode : DevExpress.XtraEditors.XtraForm
    {
        byte[] zplByte = new byte[1024 * 10];
        string zpl = "";
        //private String URL = "http://localhost:8000/api/v1/";

        public frmBarcode()
        {
            InitializeComponent();

        }

        public void zerbaLayer()
        {
           
            //zpl = File.ReadAllText("a.txt");
            zpl = "^XA^FXSecond section with recipient address and permit information.^CFA,50^FO100,300^FD"+txtSupplier.Text+"^FS^FO100,360^FD"+txtQuationNo.Text+"^FS^FO100,420^FD"+txtDate.Text+"^FS^FO100,480^FDUnited States(USA)^FS^CFA,15^FO0,570^GB1300,1,3^FS^FX Third section with barcode.^BY5,2,300^FO300,630^BC^FD"+txtQuationNo.Text+txtDate.Text+"^FS^FO0,1000^GB1300,1,3^FS^XZ";
            zplByte = Encoding.UTF8.GetBytes(zpl);

            // adjust print density (8dpmm), label width (4 inches), label height (6 inches), and label index (0) as necessary
            var request = (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/12dpmm/labels/4x6/0/");
            request.Method = "POST";
            request.Accept = "application/pdf"; // omit this line to get PNG images back
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = zplByte.Length;
            var requestStream = request.GetRequestStream();
            requestStream.Write(zplByte, 0, zplByte.Length);
            requestStream.Close();
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                var responseStream = response.GetResponseStream();
                var fileStream = File.Create("image1.pdf"); // change file name for PNG images
                responseStream.CopyTo(fileStream);
                responseStream.Close();
                fileStream.Close();
            }
            catch (WebException e)
            {
                Console.WriteLine("Error: {0}", e.Status);
            }
        }

        private void frmBarcode_Load(object sender, EventArgs e)
        {
            txtQuationNo.Text = frmReceiptStockIns.SetValueForText1;
            int id = int.Parse(txtQuationNo.Text);
            var content = RestSharpC.execCommand1("Quotations/{id}", Method.GET, id);
            //JsonHeadStockIn result = JsonConvert.DeserializeObject<JsonHeadStockIn>(content);
            //StockIns Items = new StockIns();
            //List<StockIns> lst = new List<StockIns>();
            //lst.Add(result.Data);
            JsonQuoation result = JsonConvert.DeserializeObject<JsonQuoation>(content);            
            //Items = result.Data;
            //gridControl1.DataSource = Items;
            //txtSupplier.Text = result.Data.Select(s=>s.Suppliers.NAME).ToString();
            txtSupplier.Text = result.Data.Suppliers.NAME.ToString();
            txtDate.Text = frmReceiptStockIns.SetValueForText2;
        }

        private void btnPrintBarcode_Click(object sender, EventArgs e)
        {
            zerbaLayer();
            PrintDialog pd = new PrintDialog();
            if (pd.ShowDialog() == DialogResult.OK) Printer.SendStringToPrinter(pd.PrinterSettings.PrinterName, zpl);
            webBrowser1.Navigate(Application.StartupPath + "\\image1.pdf");
        }
    }
}