﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using RestSharp;
using StockManagement.Model;
using Newtonsoft.Json;

namespace StockManagement
{
    public partial class frmStockIn : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        //private String URL = "http://localhost:8000/api/v1/";
        public static string SetValueForText = "";
        public frmStockIn()
        {
            InitializeComponent();
        }

        //private string execCommand(string source, Method method)
        //{
        //    RestClient client = new RestClient(URL);
        //    RestRequest request = new RestRequest(source, method);
        //    IRestResponse response = client.Execute(request);
        //    request.RequestFormat = DataFormat.Json;// Execute the Request
        //    string content = response.Content;
        //    return content;
        //}

        private void frmStockIn_Load(object sender, EventArgs e)
        {
            string content =RestSharpC.execCommand("StockIns/getAll", Method.GET);
            JsonHeadStockIn result = JsonConvert.DeserializeObject<JsonHeadStockIn>(content);
            grStockIn.DataSource = result.Data;
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void btnRecipt_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmReceiptStockIns f1 = new frmReceiptStockIns();

            f1.ShowDialog();
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {

        }
    }
}