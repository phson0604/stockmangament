﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using RestSharp;
using Newtonsoft.Json;
using StockManagement.Model;

namespace StockManagement
{
    public partial class frmStockOut : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        //private String URL = "http://localhost:8000/api/v1/";
        public static string SetValueForText = "";
        public frmStockOut()
        {
            InitializeComponent();
        }

        //private string execCommand(string source, Method method)
        //{
        //    RestClient client = new RestClient(URL);
        //    RestRequest request = new RestRequest(source, method);
        //    IRestResponse response = client.Execute(request);
        //    request.RequestFormat = DataFormat.Json;// Execute the Request
        //    string content = response.Content;
        //    return content;
        //}

        private void grStockOut_Load(object sender, EventArgs e)
        {
            string content = RestSharpC.execCommand("StockOuts/getAll", Method.GET);
            JsonHeadStockOut result = JsonConvert.DeserializeObject<JsonHeadStockOut>(content);
            grStockOut.DataSource = result.Data;
        }

        private void btnReceiptStockOut_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmReceiptStockOut fm = new frmReceiptStockOut();
            fm.ShowDialog();
        }
    }
}