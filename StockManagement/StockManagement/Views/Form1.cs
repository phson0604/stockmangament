﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;
using StockManagement.Model;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace StockManagement
{
    public partial class Form1 : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        //private String URL = "http://localhost:8000/api/v1/";
        public Form1()
        {
            InitializeComponent();
        }

        //private string execCommand(string source, Method method)
        //{
        //    RestClient client = new RestClient(URL);
        //    RestRequest request = new RestRequest(source, method);
        //    IRestResponse response = client.Execute(request);
        //    request.RequestFormat = DataFormat.Json;// Execute the Request
        //    string content = response.Content;
        //    return content;
        //}

        private void ribbonControl1_Click(object sender, EventArgs e)
        {

        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string content = RestSharpC.execCommand("Inventorys", Method.GET);
            Json result = JsonConvert.DeserializeObject<Json>(content);
            gridControl1.DataSource = result.Data;
        }

       

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            int g = gridView1.FocusedRowHandle;            
            frmViewInOut f1 = new frmViewInOut();
            f1.txtKetQua.Text = gridView1.GetRowCellValue(g,"PartNumber").ToString();
            f1.ShowDialog();
            
        }

        private void btnStockIn_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmStockIn f1 = new frmStockIn();
            f1.ShowDialog();            
        }

        private void btnStockOut_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmStockOut f1 = new frmStockOut();
            f1.ShowDialog();
        }

      
    }
}
