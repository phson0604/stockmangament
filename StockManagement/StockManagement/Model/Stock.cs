﻿namespace StockManagement.Model
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the <see cref="Stock" />.
    /// </summary>
    public class Stock
    {
        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the PartNumber.
        /// </summary>
        [JsonProperty("partNumber")]
        public string PartNumber { get; set; }

        /// <summary>
        /// Gets or sets the PartName.
        /// </summary>
        [JsonProperty("partName")]
        public string PartName { get; set; }

        /// <summary>
        /// Gets or sets the Price.
        /// </summary>
        [JsonProperty("price")]
        public float Price { get; set; }

        /// <summary>
        /// Gets or sets the Currency.
        /// </summary>
        [JsonProperty("currency")]
        public string Currency { get; set; }

        /// <summary>
        /// Gets or sets the Store.
        /// </summary>
        [JsonProperty("store")]
        public string Store { get; set; }

        /// <summary>
        /// Gets or sets the Position.
        /// </summary>
        [JsonProperty("position")]
        public string Position { get; set; }

        /// <summary>
        /// Gets or sets the Unit.
        /// </summary>
        [JsonProperty("unit")]
        public string Unit { get; set; }

        /// <summary>
        /// Gets or sets the ActualQty.
        /// </summary>
        [JsonProperty("actualQty")]
        public int ActualQty { get; set; }

        /// <summary>
        /// Gets or sets the Note.
        /// </summary>
        [JsonProperty("note")]
        public string Note { get; set; }

        //[JsonProperty("createdAt")]
        //public DateTime CreatedAt { get; set; }
        /// <summary>
        /// Gets or sets the UpdatedAt.
        /// </summary>
        [JsonProperty("updatedAt")]
        public DateTime UpdatedAt { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="Json" />.
    /// </summary>
    public class Json
    {
        /// <summary>
        /// Gets or sets the Status.
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the Message.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the Data.
        /// </summary>
        [JsonProperty("data")]
        public Stocks Data { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="JsonStock" />.
    /// </summary>
    public class JsonStock
    {
        /// <summary>
        /// Gets or sets the Status.
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the Message.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the Data.
        /// </summary>
        [JsonProperty("data")]
        public Stock Data { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="Stocks" />.
    /// </summary>
    public class Stocks : List<Stock>
    {
        /// <summary>
        /// Gets or sets the Stockss.
        /// </summary>
        public string Stockss { get; set; }
    }
}
