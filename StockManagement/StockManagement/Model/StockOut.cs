﻿namespace StockManagement
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the <see cref="StockOut" />.
    /// </summary>
    public class StockOut
    {
        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the PartNumber.
        /// </summary>
        [JsonProperty("partNumber")]
        public string PartNumber { get; set; }

        /// <summary>
        /// Gets or sets the ReceiptNo.
        /// </summary>
        [JsonProperty("receiptNo")]
        public string ReceiptNo { get; set; }

        /// <summary>
        /// Gets or sets the Quantity.
        /// </summary>
        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the PoID.
        /// </summary>
        [JsonProperty("poID")]
        public int PoID { get; set; }

        /// <summary>
        /// Gets or sets the Date.
        /// </summary>
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the Note.
        /// </summary>
        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty("actualNumber")]
        public int ActualNumber { get; set; }

        /// <summary>
        /// Gets or sets the Unit.
        /// </summary>
        [JsonProperty("unit")]
        public string Unit { get; set; }

        /// <summary>
        /// Gets or sets the Evidence.
        /// </summary>
        [JsonProperty("evidence")]
        public string Evidence { get; set; }


        [JsonProperty("store")]
        public string Store { get; set; }

        [JsonProperty("position")]
        public string Position { get; set; }

        //[JsonProperty("createdAt")]
        //public DateTime CreatedAt { get; set; }
        /// <summary>
        /// Gets or sets the UpdatedAt.
        /// </summary>
        [JsonProperty("updatedAt")]
        public DateTime UpdatedAt { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="JsonHeadStockOut" />.
    /// </summary>
    public class JsonHeadStockOut
    {
        /// <summary>
        /// Gets or sets the Status.
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the Message.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the Data.
        /// </summary>
        [JsonProperty("data")]
        public StockOuts Data { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="JsonStockOut" />.
    /// </summary>
    public class JsonStockOut
    {
        /// <summary>
        /// Gets or sets the Status.
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the Message.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the Data.
        /// </summary>
        [JsonProperty("data")]
        public StockOut Data { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="StockOuts" />.
    /// </summary>
    public class StockOuts : List<StockOut>
    {
        /// <summary>
        /// Gets or sets the StockOutss.
        /// </summary>
        public string StockOutss { get; set; }
    }
}
