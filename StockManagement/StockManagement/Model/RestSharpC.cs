﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockManagement.Model
{
    public class RestSharpC
    {
        /*private*/ internal static String URL = "http://localhost:8000/api/v1/";
        /*private*/ /*public*/ internal static string execCommand(string source, Method method)
        {
            RestClient client = new RestClient(URL);
            RestRequest request = new RestRequest(source, method);
            IRestResponse response = client.Execute(request);
            request.RequestFormat = DataFormat.Json;// Execute the Request
            string content = response.Content;
            return content;
        }
        /*private*/ internal static string execCommand1(string source, Method method, int id)
        {
            RestClient client = new RestClient(URL);
            RestRequest request = new RestRequest(source, method);
            request.AddUrlSegment("id", id);
            request.RequestFormat = DataFormat.Json;
            IRestResponse response = client.Execute(request);  // Execute the Request
            string content = response.Content;
            return content;
        }

       /* private */internal static string execCommand2(string source, Method method, int quotationID)
        {
            RestClient client = new RestClient(URL);
            RestRequest request = new RestRequest(source, method);
            request.AddUrlSegment("quotationID", quotationID);
            request.RequestFormat = DataFormat.Json;
            IRestResponse response = client.Execute(request);  // Execute the Request
            string content = response.Content;
            return content;
        }



        /*private*/ internal static void execCommand3(string source, Method method, StockIn stockin)
        {
            var client = new RestClient(URL);
            var request = new RestRequest(source, method);
            var json = JsonConvert.SerializeObject(stockin);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);  // Execute the Request
        }

        /*private*/ internal static void execCommand4(string source, Method method, StockIn[] stockin)
        {
            var client = new RestClient(URL);
            var request = new RestRequest(source, method);
            var json = JsonConvert.SerializeObject(stockin);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);  // Execute the Request
        }



        /* private */
        internal static string execCommand5(string source, Method method, int poID)
        {
            RestClient client = new RestClient(URL);
            RestRequest request = new RestRequest(source, method);
            request.AddUrlSegment("poID", poID);
            request.RequestFormat = DataFormat.Json;
            IRestResponse response = client.Execute(request);  // Execute the Request
            string content = response.Content;
            return content;
        }

        internal static string execCommand7(string source, Method method, string PoNumber)
        {
            RestClient client = new RestClient(URL);
            RestRequest request = new RestRequest(source, method);
            request.AddUrlSegment("PoNumber", PoNumber);
            request.RequestFormat = DataFormat.Json;
            IRestResponse response = client.Execute(request);  // Execute the Request
            string content = response.Content;
            return content;
        }

        /*private*/
        internal static void execCommand6(string source, Method method, StockOut[] stockout)
        {
            var client = new RestClient(URL);
            var request = new RestRequest(source, method);
            var json = JsonConvert.SerializeObject(stockout);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);  // Execute the Request
        }


    }

}
