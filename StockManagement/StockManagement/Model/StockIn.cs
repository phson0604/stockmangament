﻿namespace StockManagement
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the <see cref="StockIn" />.
    /// </summary>
    public class StockIn 
    {
        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the PartNumber.
        /// </summary>
        [JsonProperty("partNumber")]
        public string PartNumber { get; set; }

        /// <summary>
        /// Gets or sets the ReceiptNo.
        /// </summary>
        [JsonProperty("receiptNo")]
        public string ReceiptNo { get; set; }

        /// <summary>
        /// Gets or sets the Quantity.
        /// </summary>
        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the QuotationID.
        /// </summary>
        [JsonProperty("quotationID")]
        public int QuotationID { get; set; }

        /// <summary>
        /// Gets or sets the Date.
        /// </summary>
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the Note.
        /// </summary>
        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty("actualNumber")]
        public int ActualNumber { get; set; }

        /// <summary>
        /// Gets or sets the Unit.
        /// </summary>
        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("store")]
        public string Store { get; set; }

        [JsonProperty("position")]
        public string Position { get; set; }

        /// <summary>
        /// Gets or sets the CreatedAt.
        /// </summary>
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets the UpdatedAt.
        /// </summary>
        [JsonProperty("updatedAt")]
        public DateTime UpdatedAt { get; set; }

       
    }

    /// <summary>
    /// Defines the <see cref="JsonHeadStockIn" />.
    /// </summary>
    public class JsonHeadStockIn
    {
        /// <summary>
        /// Gets or sets the Status.
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the Message.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the Data.
        /// </summary>
        [JsonProperty("data")]
        public StockIns Data { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="JsonStockIn" />.
    /// </summary>
    public class JsonStockIn
    {
        /// <summary>
        /// Gets or sets the Status.
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the Message.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the Data.
        /// </summary>
        [JsonProperty("data")]
        public StockIn Data { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="StockIns" />.
    /// </summary>
    public class StockIns : List<StockIn>
    {
        /// <summary>
        /// Gets or sets the StockInss.
        /// </summary>
        public string StockInss { get; set; }
    }
    
}
