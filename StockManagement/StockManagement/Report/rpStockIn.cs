﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using DevExpress.Office.Utils;
using StockManagement.Model;
using System.IO;
using System.Data;

namespace StockManagement
{
    public partial class rpStockIn : DevExpress.XtraReports.UI.XtraReport
    {
        int total = 0;
        public rpStockIn(/*QuoationItems LS*/List<QuoationItem>qt, string sup,string rp,string name,string number)
        {
            InitializeComponent();
            objectDataSource1.DataSource = qt /*LS*/;
            xrLabel16.Text = sup;
            xrLabel50.Text = rp;
            xrLabel52.Text = name;
            xrLabel54.Text = number;        
            foreach (QuoationItem i in qt)
            {
                total += i.ActualNumber * i.UnitPrice;
            }
            xrLabel55.Text = ConvertNumbers.ChuyenSoSangChuoi(double.Parse(total.ToString()));
        }

        private void xrLabel2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }
    }
}
